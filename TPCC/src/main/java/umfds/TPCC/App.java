package umfds.TPCC;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class App 
{
    public static void main( String[] args ) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Groupe groupe1 = new Groupe("grp1");
        Sujet sujet1 = new Sujet("sjt1");
        groupe1.setSujet(sujet1);
        groupe1.setVoeux("Mon voeux");
        objectMapper.writeValue(new File("groupe1.json"), groupe1);
        System.out.println("Fini");
    }
}

