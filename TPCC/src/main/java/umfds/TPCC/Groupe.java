package umfds.TPCC;
import java.util.ArrayList;

public class Groupe {
    private String nom;
    private Sujet sujet;
    private String voeux;

    public Groupe(String nom) {
        this.nom = nom;
        this.sujet = null;
        this.voeux = null;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setSujet(Sujet sujet) {
        this.sujet = sujet;
        sujet.setGroupe(this);
    }

    public void setVoeux(String voeux) {
        this.voeux = voeux;
    }

    public String getNom() {
        return nom;
    }

    public Sujet getSujet() {
        return sujet;
    }

    public String getVoeux() {
        return voeux;
    }
}
