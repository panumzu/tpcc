package umfds.TPCC;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class GroupeTest {
	
	Groupe groupe1 = new Groupe("groupe1");
	Sujet sujet1 = new Sujet("sujet1");
	
	
//Test en partant de l'hypothèse que le nom d'un groupe et d'un sujet est unique
	@Test
	public void affectationSujetTest() {
		groupe1.setSujet(sujet1);
		assertEquals(groupe1.getSujet().getNom(), sujet1.getNom());
		assertEquals(groupe1.getNom(),  sujet1.getGroupe().getNom());
	}
}
